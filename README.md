# Schmemory


* Finding awesome cat/ninja/catninja pics to put on the cards. Static or dynamic?
  
  > I put 10 images in src\images\* statically. 

* Does a player get points? Is there timing involved? A scoreboard?
  
  > Yes, the player gets point according to find matching images but unfortunately there is 
  no any scoreboard. On future steps can be added to project. 

* Fancy animations or pure-and-simple?
  
  > Frankly, it's simple project so that just I figure out proof of concept of the project.

* Single-player? Local multi-player? Networked multi-player?

  > Yes it has design for single player game, I could not added fancy the futures, but on futures can be added.

* Accessibility?

  > Previously, I did not develop any application or services for disabled people, so I do not know how to-do. 

* Responsive design that works for mobile and desktop?

  > Obliviously it was the most important concern for me, during the developing process I have concerned
  the responsive design and it works for mobile and dekstop. 

# Installation of The Schmemory 

  ```javascript

    git clone git@gitlab.etude.eisti.fr:ozkanaytac/schmemory.git
    cd schmemory
    yarn install
    yarn start
  
  ```
<p>Browse to http://localhost:3000
<p>Click the cards to flip to images<p>


# Techniques and Approaches 

During the developing process, I used react-redux and component base structures on the application, 
So that I created CardView component independently from Application's base components also try to reusable it with independent events. 
It was important concern because when I wrote a component, I able to use it again and again. 

.
├── App
├── Game
├── CardView 
│   ├── cardFunctions
│   └── start.sh
└── redux
    ├── actions
    ├── store

